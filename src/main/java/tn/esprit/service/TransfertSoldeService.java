/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import tn.esprit.exception.VersementException;

@Service("transfertService")
public class TransfertSoldeService {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * Cette méthode permet de transférer un montant x d'un client a un autre.
	 * @param idEmetteur
	 * @param idBeneficiaire
	 * @param montant
	 */
	public void transfert(Long idEmetteur, Long idBeneficiaire, Long montant){
		//Retrait
		Long ancienSolde = jdbcTemplate.queryForObject(
				"select solde from Client where id=?", Long.class, idEmetteur);
		Long nouveauSolde = ancienSolde - montant;
		jdbcTemplate.update(
				"update Client SET solde=? where id=?", nouveauSolde, idEmetteur);
		
		/*******POUR SIMULER UNE ERREUR AU COURS DU VERSEMENT*************************/
		if (nouveauSolde > -1000) {
			throw new VersementException();
		}
		/**************************************************************************/
		
		//Versement
		ancienSolde = jdbcTemplate.queryForObject(
				"select solde from Client where id=?", Long.class, idBeneficiaire);
		nouveauSolde = ancienSolde + montant;

		jdbcTemplate.update(
				"update Client SET solde=? where id=?", nouveauSolde, idBeneficiaire);
	}
	
}


