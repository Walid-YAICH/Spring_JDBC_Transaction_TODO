-- Server version: 10.1.34-MariaDB
-- InnoDB is a good general transaction storage engine. It is the default MySQL storage engine.
-- https://mariadb.com/kb/en/library/start-transaction/
-- https://stackoverflow.com/questions/2950676/difference-between-set-autocommit-1-and-start-transaction-in-mysql-have-i-misse
SET AUTOCOMMIT = 0;
-- DDL (Data Definition Language) statements are automatically committed - 
-- they do not require a COMMIT statement.
CREATE TABLE `Client` (
  `id` int(100) NOT NULL,
  `solde` int(100) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data for table `Client`
--
INSERT INTO `Client` (`id`, `firstName`, `lastName`) VALUES
(1, 'Mohamed', 'BOUZID'),
(2, 'Salah', 'ZITOUNI');

COMMIT;


